��    _                      	  #        ;  
   B     M     \     n     �     �     �     �  
   �  
   �     �     �     �     �     �     �     	     	     	     "	     /	     A	  	   G	     Q	     ^	     d	     m	     {	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	  :   �	  K   &
  #   r
  =   �
  )   �
     �
          ,     2     A     H     T     f  '   s  8   �     �     �  -   �          '     7     S     e  	   |     �     �     �     �     �  )   �     �          "  !   4  K   V  1   �     �  8   �          "     7     E     R  
   _  `   j  b   �     .     6  	   =     G     a  �  x  
   !  V   ,     �     �     �     �  ,   �  
   �     �  
                  )     5     I     V     e     w     �     �     �     �     �     �     �     	  %        >     E     Y     m     v     �     �     �  
   �     �     �     �          	       
     Q   )  q   {  G   �  �   5  C   �       1        M     Z     y     �     �  4   �  <   �  `   $  4   �      �  6   �            -   3      a  /   �     �     �     �  9   �  <     
   M  9   X     �     �      �  K   �  |   -  `   �  
     [        r      �     �     �     �     �  �   �  �   �     �     �     �  '   �  !   �     J       &   Q              H       (   U         T          9   4   .          D   '               #       K   2      ^       ;             C   W               )       *           ]   \   +            	   6       S         0   F      _   3           7          B   P      Y          1       E       5   >   X       [   <   Z      A      %             /   @          I   :   G       -       ?   M      "   V              ,   
   O      L   N          R      =   8   !      $     confirmation  is not a valid Lightning invoice:  &About &Addresses &Documentation &Donate to server &Encrypt/decrypt message &Export &File &Filter &From QR code &From file &From text &From the blockchain &Help &History &Import &Information &Labels &Load transaction &Network &New &New/Restore &Official website &Open &Password &Pay to many &Plot &Plugins &Private keys &Quit &Recently open &Report Bug &Seed &Sign/verify message &Summary &Tools &View &Wallet + 1 day 1 hour 1 week 1. Place this paper on a flat and well iluminated surface. 2. Align your Revealer borderlines to the dashed lines on the top and left. 4. Type the numbers in the software A backup is saved automatically when generating a new wallet. A copy of your wallet file was created in A few examples A library is probably missing. About About Electrum Accept Accept Word Acquisition price Add Cosigner Add a cosigner to your multi-sig wallet Add an optional virtual keyboard to the password dialog. Add cosigner Additional fees Additional {} satoshis are going to be added. Address Address Details Address copied to clipboard Address is frozen Address not in wallet. Addresses Advanced All All fields must be filled out Always check your backups. Amount Amount for OP_RETURN output must be zero. Amount to be sent Amount to be sent. An Error Occurred Are you sure you want to proceed? Are you sure you want to remove this transaction and {} child transactions? Are you sure you want to remove this transaction? Armenian At most 100 satoshis might be lost due to this rounding. Audio MODEM Audio Modem Settings Authorization Auto connect Auto-connect BIP39 seed BIP39 seeds can be imported in Electrum, so that users can access funds locked in other wallets. BIP39 seeds do not include a version number, which compromises compatibility with future software. Balance Banner Base unit Base unit of your wallet. Batch RBF transactions Project-Id-Version: electrum
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-07-08 00:01
Last-Translator: 
Language-Team: Persian
Language: fa_IR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Crowdin-Project: electrum
X-Crowdin-Project-ID: 20482
X-Crowdin-Language: fa
X-Crowdin-File: /electrum-client/messages.pot
X-Crowdin-File-ID: 68
 تصدیق  این یک صورتحساب قابل قبول ،شبکه لایتنینگ نیست:  درباره آدرس ها مستندات کمک به سرور رمزنگاری/رمزگشایی پیغام خروجی فایل فیلتر از QR کد از فایل از متن از بلاکچین راهنما تاریخچه وارد کردن اطلاعات برچسب ها لود کردن تراکنش شبکه جدید جدید/بازگردانی وب‌سایت رسمی بازکردن رمزعبور پرداخت به چندین حساب طرح افزونه‌ها کلید خصوصی خروج به تازگی باز شده گزارش مشکلات سید امضا/تایید پیغام خلاصه ابزارها مشاهده کیف پول + 1 روز ۱ ساعت 1 هفته ۱. این کاغذ را در یک سطح مسطح و روشن قرار دهید. کادر رویلر را در بالا و سمت چب بر روی خطوط، نقطه چین تنظیم کنید. ۴. اعداد موجود در نرم افزار را تایپ کنید زمانی که یک کیف پول جدید ایجاد می شود، یک فایل پشتیبان خودکار نیز ذخیره می شود. یک کپی از فایل کیف پول شما ذخیره شد در چندین مثال یک کتابخانه احتمالا گم شده. درباره درباره ی الکتروم پذیرفتن پذیرفتن کلمه قیمت خرید اضافه نمودن شریک دارای امضاء اضافه کردن شریک به کیف چند امضائی اضافه کردن دلخواه یک کیبرد مجازی به پنجره دریافت رمز. اضافه نمودن شریک دارای امضاء هزینه های افزایشی مقدار {} ساتوشی اضافه خواهد شد. آدرس اطلاعات آدرس آدرس در حافظه موقت کپی شد این آدرس بسته شده  این آدرس در کیف وجود ندارد آدرس ها پیشرفته همه تمامی کادر ها میبایست وارد شوند همیشه وجود پشتیبان را کنترل کنید. مقدار همیشه مقدار OP_RETURN باید صفر باشد. مقدار ارسال  مقدار ارسال. يك خطا رخ داده است آیا مطمئن هستید که می‌خواهید ادامه دهید؟ آیا مطمئن هستید که می‌خواهید این تراکنش و زیر تراکنش ها را پاک کنید؟ آیا مطمئن هستید که می‌خواهید این تراکنش را پاک کنید؟ ارمنی ممکن است با رند کردن حداکثر ۱۰۰ ساتوشی از بین برود. مودم صوتی تنظیمات مودم صوتی مجوز اتصال خودکار اتصال-خودکار سید BIP39 سید های BIP39 می توانند در الکتروم وارد شوند، پس کاربران میتوانند به پول های قفل شده ی خود در کیف پول دسترسی پیدا کنند. سید های BIP39 شامل ورژن های متفاوت نمی شوند، چیزی که باعث می شود که با نسخه های آینده ی نرم افزار هم سازگاری داشته باشد. موجودی پرچم واحد پایه واحد اصلی کیف پول شما. تراکنشهای RBF گروهی 